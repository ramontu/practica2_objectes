import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;

import acm.program.ConsoleProgram;

public class Main extends ConsoleProgram {

	private static final String CHARACTERS_FILES = "characters.txt";
	private static final String CHARACTERS_DB_NAME = "charactersDB.dat";
	private CharactersDB charactersDB;

	public void run() {
		try {
			charactersDB = new CharactersDB (CHARACTERS_DB_NAME);
			loadFromFiles();
		} catch (IOException ex) {
			println ("Error generating database!");
			System.exit (-1);
		}
		for (;;) {
			printMenu();
			int option = getOption();
			switch (option) {
				case 1:
					listNames();
					break;
				case 2:
					infoFromOneCharacter();
					break;
				case 3:
					deleteCharacter();
					break;
				case 4:
					quit();
					break;
			}
			println();
		}
	}

	private void printMenu() {
		println ("Menú d'opcions:");
		println ("1 - Llista tots els noms de personatge.");
		println ("2 - Obté la informació d'un personatge.");
		println ("3 - Elimina un personatge.");
		println ("4 - Sortir.");
	}

	private int getOption() {
		int option;
		do {
			option = readInt ("Escull una opció: ");
		} while (option <= 0 || option > 4);
		return option;
	}

	private void loadFromFiles() throws IOException {
		charactersDB.reset();
		BufferedReader input = new BufferedReader (new FileReader (CHARACTERS_FILES));
		String fileName = input.readLine();
		while (fileName != null) {
			CharacterInfo character = CharacterInfoReader.readCharacterFile (fileName);
			charactersDB.appendCharacterInfo (character);
			fileName = input.readLine();
		}
		input.close();
	}

	private void listNames() {
		//###################################################################
		//				NO TOCAR, FUNCIONA
		//###################################################################
		int numCharacters = charactersDB.getNumCharacters();
		try {
			for (int i = 0; i < numCharacters; i++) {
				println (charactersDB.readCharacterInfo(i).getName());
			}


		} catch (IOException ex) {
			println ("Database error! No es pot fer la llista");
		}


	}

	private void infoFromOneCharacter() {
		String name = readLine ("Escriu el nom del personatge: ");
		try {
			int n = charactersDB.searchCharacterByName (name);
			if (n != -1) {
				CharacterInfo character = charactersDB.readCharacterInfo (n);
				println (character);
			} else {
				println ("Personatge no trobat.");
			}
		} catch (IOException ex) {
			println ("Database error!");
		}
	}

	private void deleteCharacter() {
		//###################################################################
		//				NO TOCAR, FUNCIONA
		//###################################################################
		String name = readLine ("Escriu el nom del personatge a eliminar: ");
		try {
			boolean success = charactersDB.deleteByName (name);
			if (!success) {
				println ("Personatge no trobat.");
			}
		} catch (IOException ex) {
			println ("Database error!");
		}
	}

	private void quit() {
		try {
			charactersDB.close();
			System.exit (0);
		} catch (IOException ex) {
			println ("Error closing database!");
			System.exit (-1);
		}
	}

}
