import javax.swing.text.html.parser.Parser;

public class CharacterInfo {

	private String  name;
	private boolean hasYChromosome;
	private boolean canDoMagic;

	// For the following stats, the expected value for a human is 100
	private int intelligence;
	private int strength;
	private int constitution;

	private static final int NAME_LIMIT = 25;
	public  static final int SIZE = (NAME_LIMIT*2+4*3+2);

	public CharacterInfo (String name, boolean hasYChromosome, boolean canDoMagic, int intelligence, int strength, int constitution) {
		this.name           = name;
		this.hasYChromosome = hasYChromosome;
		this.canDoMagic     = canDoMagic;
		this.intelligence   = intelligence;
		this.strength       = strength;
		this.constitution   = constitution;
	}

	// Getters
	public String  getName          () { return name;           }
	public boolean getHasYChromosome() { return hasYChromosome; }
	public boolean getCanDoMagic    () { return canDoMagic;     }
	public   int   getIntelligence  () { return intelligence;   }
	public   int   getStrength      () { return strength;       }
	public   int   getConstitution  () { return constitution;   }

	public byte[] toBytes() {
		byte[] record = new byte[SIZE];
		PackUtils.packLimitedString(getName(),50,record,0);
		PackUtils.packBoolean(getHasYChromosome(),record,50);
		PackUtils.packBoolean(getCanDoMagic(),record,51);
		PackUtils.packInt(getIntelligence(),record,52);
		PackUtils.packInt(getStrength(),record,56);
		PackUtils.packInt(getConstitution(),record,60);
		return record;
	}

	public static CharacterInfo fromBytes (byte[] record) {  //FUNCIONA

		String nombre = PackUtils.unpackLimitedString(50,record,0);
		boolean genere = PackUtils.unpackBoolean(record,50);
		boolean magia = PackUtils.unpackBoolean(record,51);
		int inteligencia = PackUtils.unpackInt(record,52);
		int forca = PackUtils.unpackInt(record,56);
		int constitucio = PackUtils.unpackInt(record,60);
		return new CharacterInfo(nombre, genere, magia,inteligencia,forca,constitucio);
	}

	public String toString() {
		String result = name;
		if (hasYChromosome) {
			result += ", gènere: XY";
		} else {
			result += ", gènere: XX";
		}
		result += System.lineSeparator() +
			"Intel·ligència: " + intelligence + System.lineSeparator() +
			"Força:          " + strength     + System.lineSeparator() +
			"Constitució:    " + constitution + System.lineSeparator();
		if (canDoMagic) {
			result += "Pot fer màgia.";
		} else {
			result += "No pot fer màgia.";
		}
		return result;
	}

}
