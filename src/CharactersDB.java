import javax.swing.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.rmi.UnexpectedException;

public class CharactersDB {

	private RandomAccessFile charactersDB;
	private int numCharacters;

	public CharactersDB (String fileName) throws IOException {
		charactersDB = new RandomAccessFile (fileName, "rw");
		numCharacters = (int)charactersDB.length() / CharacterInfo.SIZE;
	}

	public int getNumCharacters() {
		return numCharacters;
	}

	public void close() throws IOException {
		charactersDB.close();
	}

	public void reset() throws IOException {
		charactersDB.setLength (0);
		numCharacters = 0;
	}

	public CharacterInfo readCharacterInfo (int n) throws IOException {
		//###################################################################
		//				NO TOCAR, FUNCIONA
		//###################################################################
		try {
			RandomAccessFile input;
			input = new RandomAccessFile("CharactersDB.dat","rw");
			byte[] entrada = new byte[CharacterInfo.SIZE*(numCharacters+1)];
			for (int i = 0; i<numCharacters*64;i++){
				entrada[i] = (byte)input.read();
			}
			byte[] selecionat = new byte[64];
			for (int i = 0; i<64; i++){
				selecionat[i] = entrada[(n*64+i)];
			}
			return CharacterInfo.fromBytes(selecionat);
		}catch (IOException ex) {
			throw new IOException("Error en readcharacter");
		}


	}

	public int searchCharacterByName (String name) throws IOException {

		for (int i = 0; (i<=numCharacters);i++){
			String nom =readCharacterInfo(i).getName();
			if (nom.equalsIgnoreCase(name)){
				return i;
			}
		}
		throw new IOException("No es troba el personatge");
	}

	public void writeCharacterInfo (int n, CharacterInfo character) throws IOException {
		//###################################################################
		//				NO TOCAR, FUNCIONA
		//###################################################################
		RandomAccessFile output;
		output = new RandomAccessFile ("CharactersDB.dat","rw");
		byte[] inchar =character.toBytes();
		byte[] sortida = new byte[64*(numCharacters+1)];

		for (int i = 0; i<(numCharacters+1)*64; i++){
			sortida[i]=(byte) output.read();
		}
		int pos1 = n*64;
		for (int i = 0; i<64; i++,pos1++){
			sortida[pos1]=inchar[i];
		}
		output.setLength(0);
		output.write(sortida);
		output.close();
	}


	public void appendCharacterInfo (CharacterInfo character) throws IOException {
		writeCharacterInfo (numCharacters, character);
		numCharacters++;
	}

	public boolean deleteByName (String name) throws IOException {
		int num = searchCharacterByName(name);
		RandomAccessFile input;
		input = new RandomAccessFile("CharactersDB.dat","rw");
		byte[] entrada = new byte[64*(numCharacters+1)];
		for (int i = 0; i< entrada.length; i++){
			entrada[i] = (byte)input.read();
		}
		for (int i = 64*(num); i<(64*(numCharacters-1)); i++){
			entrada[i]=entrada[i+64];
		}
		numCharacters--;
		input.setLength(0);

		input.write(entrada);
		input.setLength(numCharacters*64);

		return true;
	}

}
