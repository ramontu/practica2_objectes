import acm.program.ConsoleProgram;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;


public class CharacterInfoReader{

	public static CharacterInfo readCharacterFile (String fileName) throws IOException {

		try{
			//###################################################################
			//				NO TOCAR, FUNCIONA
			//###################################################################
			RandomAccessFile input;
			input = new RandomAccessFile (fileName,"rw");
			String nombre = llegir_intro(input);
			String generetext = llegir_intro(input);
			String temagia = llegir_intro(input);
			String inteli =llegir_intro(input);
			String forca = llegir_intro(input);
			String consti = llegir_intro(input);

			int inteligencia = aint(inteli);
			int forcai = aint(forca);
			int constitucio= aint(consti);

			boolean magia = false;
			if (temagia.toLowerCase().equals("yes")) {
				magia = true;
			}

			boolean genere = false;
			for (int i = 0; i<generetext.length()&&!genere;i++){
				if (generetext.toLowerCase().charAt(i) == 'y' ){
					genere =true;
				}
			}
			CharacterInfo persona = new CharacterInfo(nombre,genere,magia,inteligencia,forcai,constitucio);
			input.close();
			return persona;
		}catch (IOException ex){
			System.out.println("Error llegint el fitxer "+fileName);
			return new CharacterInfo("",false,false,100,100,100);
		}
		


	}
	
	public static String llegir_intro  (RandomAccessFile entrada) throws IOException{
		String input = "";
		char a = (char)entrada.read();
		while (a != 10){
			input = input+a;
			a = (char)entrada.read();
		}
		return input;
	}
	private static int aint(String entrada){
		int multiplicador = 1;
		int sortida = 0;
		for (int i = entrada.length()-1; i >= 0; i--){
			sortida = sortida + (entrada.charAt(i)-48)*multiplicador;
			multiplicador = multiplicador*10;
		}
		return sortida;
	}


}
